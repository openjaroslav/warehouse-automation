# Warehouse automation

Разработка системы складского учета с выдачей товара по QR коду.

Для запуска:

* git clone https://gitlab.com/Adreneidos/warehouse-automation.git
* pip install pipenv
* pipenv shell
* pipenv install --dev
* python manage.py makemigrations 
* python manage.py migrate
* python manage.py runserver
