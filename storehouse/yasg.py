from django.conf.urls import url
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions


API_TITLE = 'Warehouse Automation'
API_VERSION = 'v1'
API_DESCRIPTION = 'Схема API системы складского учета с выдачей товара по QR-коду'
schema_view = get_schema_view(
    openapi.Info(
        title=API_TITLE,
        default_version=API_VERSION,
        description=API_DESCRIPTION,
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
