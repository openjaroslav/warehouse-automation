from django.db import models

from storehouse.products.models import Category, Product


class Provider(models.Model):
    """
    Поставщик
    """
    name = models.CharField(max_length=100,
                            verbose_name='Наименование поставщика')
    requisites = models.TextField(verbose_name='Реквизиты')
    contact_person = models.CharField(max_length=100,
                                      verbose_name='Контаткное лицо')
    phone = models.CharField(max_length=20,
                             verbose_name='Телефон')
    email = models.EmailField(verbose_name='Электронная почта')
    categories = models.ManyToManyField(Category,
                                        blank=True,
                                        verbose_name='Категории привозимых товаров')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Поставщик",
        verbose_name_plural = "Поставщики"


class Delivery(models.Model):
    """
    Поставка
    """
    STATUSES = (
        ('1', 'Новая'),
        ('2', 'В пути'),
        ('3', 'Доставлено на склад'),
        ('4', 'Оприходовано складом'),
    )
    code_delivery = models.IntegerField(verbose_name='Код поставки')
    provider = models.ForeignKey(Provider,
                                 on_delete=models.PROTECT,
                                 verbose_name="Поставщик",
                                 related_name='deliveries', )
    number_items = models.PositiveIntegerField(verbose_name='Количетсво позиций')
    total_price = models.DecimalField(max_digits=10,
                                      decimal_places=2,
                                      verbose_name='Сумма поставки')
    registered = models.DateTimeField(verbose_name='Регистрацияя поставки')
    status_updated = models.DateTimeField(verbose_name='Последнее изменение статуса')
    status = models.CharField(max_length=50,
                              choices=STATUSES,
                              default='1',
                              verbose_name='Статус')

    def __str__(self):
        return f'Поставка {self.code_delivery}  {self.status}'

    class Meta:
        verbose_name = 'Поставка'
        verbose_name_plural = 'Поставки'


class ProductInDelivery(models.Model):
    """
    Товары в поставке
    """
    delivery = models.ForeignKey(Delivery,
                                 blank=True,
                                 null=True,
                                 default=None,
                                 on_delete=models.CASCADE,
                                 verbose_name='Поставка')
    product = models.ForeignKey(Product,  # товар
                                blank=True,
                                null=True,
                                default=None,
                                on_delete=models.CASCADE,
                                verbose_name='Товар')
    quantity = models.PositiveIntegerField(default=1,
                                           verbose_name='Количество товара')
    price_item = models.DecimalField(max_digits=10,
                                     decimal_places=2,
                                     verbose_name='Цена за ед.')
    total_price = models.DecimalField(max_digits=10,
                                      decimal_places=2,
                                      verbose_name='Сумма')

    def __str__(self):
        return f'{self.product.name}'

    class Meta:
        verbose_name = 'Товар в поставке'
        verbose_name_plural = 'Товары в поставке'
