from django.apps import AppConfig


class DeliveriesConfig(AppConfig):
    name = 'deliveries'
    verbose_name = 'Поставки'
