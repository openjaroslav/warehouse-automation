from django.shortcuts import render
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet

from storehouse.orders.models import Order, Customer
from storehouse.orders.serializers import OrderSerializer, CustomerSerializer, OrderDetailSerializer

class OrderFilter(FilterSet):
    class Meta:
        model = Order
        fields = ('number_items', 'total_price',)

class OrderView(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = OrderFilter


class OrderDetailView(generics.ListCreateAPIView):
    queryset = Order.objects.select_related('products_in_order').all()
    serializer_class = OrderDetailSerializer


class CustomerView(generics.ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


