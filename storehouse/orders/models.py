from django.db import models

from storehouse.products.models import Product


class Customer(models.Model):
    """
    Поставщик
    """
    first_name = models.CharField(max_length=100,
                                  verbose_name='Имя')
    second_name = models.CharField(max_length=100,
                                   null=True,
                                   verbose_name='Отчество')
    surname = models.CharField(max_length=100,
                               verbose_name='Фамилия')
    contact_person = models.CharField(max_length=100,
                                      verbose_name='Контактное лицо')
    phone = models.CharField(max_length=15,
                             verbose_name='Телефон')
    email = models.EmailField(verbose_name='Электронная почта')

    def __str__(self):
        return f'{self.first_name[0]}.{self.second_name[0]}. {self.surname}'

    class Meta:
        verbose_name = "Покупатель",
        verbose_name_plural = "Покупатель"


class Order(models.Model):
    """
    Заказ
    """
    STATUSES = (
        ('1', 'Новый'),
        ('2', 'Проверка на складе'),
        ('3', 'Отменен'),
        ('4', 'Сборка заказа'),
        ('5', 'Ожидание отгрузки'),
        ('6', 'Выдан покупателю'),
    )
    code_order = models.IntegerField(verbose_name='Номер заказа')
    customer = models.ForeignKey(Customer,
                                 on_delete=models.PROTECT,
                                 verbose_name='Покупатель',
                                 related_name='orders',)
    number_items = models.IntegerField(verbose_name='Количество позиций')
    total_price = models.DecimalField(max_digits=10,
                                      decimal_places=2,
                                      verbose_name='Сумма заказа')
    registered = models.DateTimeField(verbose_name='Регистрация заказа')
    status_updated = models.DateTimeField(verbose_name='Изменение статуса заказа')
    status = models.CharField(max_length=50,
                              choices=STATUSES,
                              default='1',
                              verbose_name='Статус заказа')

    def __str__(self):
        return f'Заказ {self.code_order}  {self.status}'

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'


class ProductInOrder(models.Model):
    """
    Товар в заказе
    """
    order = models.ForeignKey(Order,
                              blank=True,
                              null=True,
                              default=None,
                              on_delete=models.CASCADE,
                              verbose_name='Заказ',
                              related_name='products_in_order')
    product = models.ForeignKey(Product,
                                blank=True,
                                null=True,
                                default=None,
                                on_delete=models.CASCADE,
                                verbose_name='Товар')
    quantity = models.PositiveIntegerField(default=1,
                                           verbose_name='Количество')
    price_item = models.DecimalField(max_digits=10,
                                     decimal_places=2,
                                     verbose_name='Цена за ед.')
    total_price = models.DecimalField(max_digits=10,
                                      decimal_places=2,
                                      verbose_name='Сумма')

    def __str__(self):
        return f'{self.product.name}'

    class Meta:
        verbose_name = 'Товар в заказе'
        verbose_name_plural = 'Товары в заказе'
