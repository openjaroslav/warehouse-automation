# Generated by Django 3.1 on 2020-08-06 23:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20200807_0917'),
        ('orders', '0004_remove_order_customer_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='contact_person',
            field=models.CharField(max_length=100, verbose_name='Контактное лицо'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='email',
            field=models.EmailField(max_length=254, verbose_name='Электронная почта'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='first_name',
            field=models.CharField(max_length=100, verbose_name='Имя'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=models.CharField(max_length=15, verbose_name='Телефон'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='second_name',
            field=models.CharField(max_length=100, null=True, verbose_name='Отчество'),
        ),
        migrations.AlterField(
            model_name='customer',
            name='surname',
            field=models.CharField(max_length=100, verbose_name='Фамилия'),
        ),
        migrations.AlterField(
            model_name='order',
            name='code_order',
            field=models.IntegerField(verbose_name='Номер заказа'),
        ),
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='orders', to='orders.customer', verbose_name='Покупатель'),
        ),
        migrations.AlterField(
            model_name='order',
            name='number_items',
            field=models.IntegerField(verbose_name='Количество позиций'),
        ),
        migrations.AlterField(
            model_name='order',
            name='registered',
            field=models.DateTimeField(verbose_name='Регистрация заказа'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(choices=[('1', 'Новый'), ('2', 'Проверка на складе'), ('3', 'Отменен'), ('4', 'Сборка заказа'), ('5', 'Ожидание отгрузки'), ('6', 'Выдан покупателю')], default='1', max_length=50, verbose_name='Статус заказа'),
        ),
        migrations.AlterField(
            model_name='order',
            name='status_updated',
            field=models.DateTimeField(verbose_name='Изменение статуса заказа'),
        ),
        migrations.AlterField(
            model_name='order',
            name='total_price',
            field=models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Сумма заказа'),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='order',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='orders.order', verbose_name='Заказ'),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='price_item',
            field=models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Цена за ед.'),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='product',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='products.product', verbose_name='Товар'),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='quantity',
            field=models.PositiveIntegerField(default=1, verbose_name='Количество'),
        ),
        migrations.AlterField(
            model_name='productinorder',
            name='total_price',
            field=models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Сумма'),
        ),
    ]
